if exists('g:loaded_spellloop')
    finish
endif
let g:loaded_spellloop = 1

" Setup a default spell list
if !exists('g:spell_list') || empty(g:spell_list)
    let g:spell_list = ['none', 'en']
endif

" External API
function! SpellLoop_Init()
    if &buftype ==# '' " Skip non-file buffers
        " Set the current language to the first in the list
        call s:set_idx(0)
    endif
endfunction

noremap <silent> <Plug>(spellloop)        :call <SID>spell_loop()<CR>
noremap <silent> <Plug>(spellloop-toggle) :call <SID>spell_loop_toggle_spell()<CR>

" Private Functions

function! s:spell_loop()
    if exists('g:spellloop_skip_nospell') && g:spellloop_skip_nospell && !&spell
        " If this option is set, ignore calls where spell is not set
        return
    endif
    let l:old_idx = index(g:spell_list, &spelllang)
    if &spell == 0 || l:old_idx == -1
        " Another language set or spell is disabled
        " Go back to the first one
        call SpellLoop_Init()
    else
        call s:set_idx((l:old_idx + 1) % len(g:spell_list))
    endif
endfunction

function! s:spell_loop_toggle_spell()
    setlocal invspell
    call s:autocmd()
endfunction

function! s:set_idx(num)
    let l:spell = g:spell_list[a:num]

    if l:spell ==# 'none'
        setlocal nospell
    else
        setlocal spell
        let &l:spelllang = l:spell
    endif

    if exists('g:spellloop_verbose') && g:spellloop_verbose
        echo 'Current Spelling Language: ' . l:spell
    endif

    call s:autocmd()
endfunction

function! s:autocmd()
    if exists('#User#SpellingLanguageChange')
        doautocmd User SpellingLanguageChange
    endif
endfunction
