# Spell Loop

Loop over different languages in Vim spell check.

This is tested on NeoVim, but it should work on a recent Vim too.

## Installation

Use your favourite plugin manager.

## Configuration

### Spell List

A array of settings for `spelllang`.

The special value `none` disables `spell`.

Defaults to:

```vim
let g:spell_list = ['none', 'en']
```

### Mapping: Loop through spelling languages

This loops thorough spelling languages defined in `g:spell_list`.

Defaults to enabling the `spell` variable if it isn't already enabled. To keep that variable as-is, define `g:spellloop_skip_nospell`.

If you want feedback on language change, define `g:spellloop_verbose`.

Exposed on the `<Plug>(spellloop)` mapping. Example:

```vim
nmap <silent> <Leader>ts <Plug>(spellloop)
```

### Mapping: Toggle spell check

This toggles the `spell` variable.

Exposed on the `<Plug>(spellloop-toggle)` mapping. Example:

```vim
nmap <silent> <Leader>tS <Plug>(spellloop-toggle)
```
